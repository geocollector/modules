# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Added

- Geocollector base module: [changelog](tr_geocollector/CHANGELOG.md)
- Geocollector collection module: [changelog](tr_geocollector_collections/CHANGELOG.md)
- Geocollector geology module: [changelog](tr_geocollector_geology/CHANGELOG.md)
- Geocollector trip module: [changelog](tr_geocollector_trips/CHANGELOG.md)
