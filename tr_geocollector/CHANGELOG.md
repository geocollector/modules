# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 16.0.1.0.0 - 2023-09-01

### Added

- Geo location type model
- Geo location model

### Changed

- Specimen discovery location has been changed from res.partner to geo.location

### Removed

- `is_geo_location` flag on `res.partner`

## 16.0.1.0.0 - 2023-08-23

### Added

- Geo specimen model
- Specimen information sheet
- Specimen label sheet
