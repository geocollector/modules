# GeoCollector: Manage your collection of rocks and fossils
# Copyright (C) 2023  Transistories

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


{
    'name': 'GeoCollector',
    'version': '16.0.1.2.0',
    'category': 'GeoCollector/GeoCollector',
    'summary': 'Manage your collection of rocks and fossils',
    'description': """""",
    'author': 'Transistories',
    'website': 'https://www.transistories.org',
    'license': 'GPL-3 or any later version',
    'depends': [
        'base_setup',
    ],
    'data': [
        'security/geocollector_security.xml',
        'security/ir.model.access.csv',

        'views/geo_location.xml',
        'views/geo_location_status.xml',
        'views/geo_location_type.xml',
        'views/geo_specimen_type.xml',
        'views/geo_specimen.xml',

        'views/res_config_settings.xml',

        'wizards/print_specimen_labels.xml',

        'views/ir_actions_act_window.xml',
        'views/menu_actions.xml',
        'views/menu_views.xml',

        'reports/report_paperformat.xml',
        'reports/ir_actions_report.xml',
        'reports/report_geo_specimen.xml',
        'reports/report_geo_specimen_label_sheet.xml',

        'data/geo_location_status.xml',
        'data/geo_location_type.xml',
        'data/geo_specimen_type.xml',
        'data/ir_sequence.xml',
    ],
    'assets': {
        'web.report_assets_common': [
            'tr_geocollector/static/src/**/*',
        ],
    },
    'installable': True,
    'application': True,
    'auto_install': False,
    'post_init_hook': 'post_init_hook',
}
