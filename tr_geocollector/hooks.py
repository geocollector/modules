from odoo import SUPERUSER_ID, api


def post_init_hook(cr, registry):
    env = api.Environment(cr, SUPERUSER_ID, {})
    main_company = env.ref('base.main_company')
    default_specimen_sequence = env.ref(
        'tr_geocollector.default_geo_specimen_sequence',
    )

    main_company.geo_specimen_sequence_id = default_specimen_sequence
