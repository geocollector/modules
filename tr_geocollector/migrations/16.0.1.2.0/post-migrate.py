from odoo import SUPERUSER_ID, api


def migrate(cr, installed_version):
    env = api.Environment(cr, SUPERUSER_ID, {})

    unknown_location_status = env.ref(
        'tr_geocollector.location_status_unknown',
        False
    )

    if unknown_location_status:
        locations = env['geo.location'].search([])
        locations.write({
            'status_id': unknown_location_status.id,
        })

    other_specimen_type = env.ref(
        'tr_geocollector.specimen_type_other',
        False
    )

    if other_specimen_type:
        specimens = env['geo.specimen'].search([])
        specimens.write({
            'type_id': other_specimen_type.id,
        })
