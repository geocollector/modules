from . import geo_location
from . import geo_location_status
from . import geo_location_type

from . import geo_specimen
from . import geo_specimen_type

from . import res_company

from . import res_config_settings
