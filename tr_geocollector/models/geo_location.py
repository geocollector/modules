from odoo import _, fields, models


class GeoLocation(models.Model):
    _name = 'geo.location'
    _description = 'Location'
    _inherit = ['format.address.mixin']

    def _default_type_id(self):
        type_id = self.env.ref(
            'tr_geocollector.geo_location_type_other',
            False,
        )
        if type_id:
            return type_id.id
        return False

    def _default_status_id(self):
        status_id = self.env.ref(
            'tr_geocollector.location_status_unknown',
            False,
        )
        if status_id:
            return status_id.id
        return False

    name = fields.Char(
        string='Name',
        required=True,
        index=True,
        copy=False,
    )
    status_id = fields.Many2one(
        string='Status',
        comodel_name='geo.location.status',
        required=True,
        default=_default_status_id,
    )
    type_id = fields.Many2one(
        string='Location Type',
        comodel_name='geo.location.type',
        required=True,
        default=_default_type_id,
    )
    partner_id = fields.Many2one(
        string='Linked Contact',
        comodel_name='res.partner',
    )

    street = fields.Char()
    street2 = fields.Char()
    zip = fields.Char(
        change_default=True,
    )
    city = fields.Char()
    state_id = fields.Many2one(
        string='State',
        comodel_name='res.country.state',
        ondelete='restrict',
        domain="[('country_id', '=?', country_id)]",
    )
    country_id = fields.Many2one(
        string='Country',
        comodel_name='res.country',
        ondelete='restrict',
    )
    country_code = fields.Char(
        string='Country Code',
        related='country_id.code',
    )
    latitude = fields.Float(
        string='Latitude',
        digits=(10, 7),
    )
    longitude = fields.Float(
        string='Longitude',
        digits=(10, 7),
    )

    note = fields.Html(
        string='Notes',
    )

    def copy(self, default=None):
        default = default or {}
        if 'name' not in default:
            default['name'] = f"{self.name} ({_('copy')})"
        return super().copy(default)

    _sql_constraints = [
        (
            "name_uniq", "unique(name)", (
                "A location already exists with this name\n"
                "You cannot define another: please edit the existing one."
            ),
        ),
    ]
