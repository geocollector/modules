from odoo import _, fields, models


class GeoLocationStatus(models.Model):
    _name = 'geo.location.status'
    _description = 'Location Status'
    _order = 'sequence, id'

    name = fields.Char(
        string='Name',
        required=True,
        index=True,
        copy=False,
    )
    sequence = fields.Integer(
        string='Sequence',
        default=10,
    )

    def copy(self, default=None):
        default = default or {}
        if 'name' not in default:
            default['name'] = f"{self.name} ({_('copy')})"
        return super().copy(default)

    _sql_constraints = [
        (
            "name_uniq", "unique(name)", (
                "A location status already exists with this name\n"
                "You cannot define another: please edit the existing one."
            ),
        ),
    ]
