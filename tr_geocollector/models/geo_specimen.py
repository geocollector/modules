from odoo import _, api, fields, models


class GeoSpecimen(models.Model):
    _name = 'geo.specimen'
    _description = 'Specimen'
    _check_company_auto = True

    def _default_type_id(self):
        type_id = self.env.ref(
            'tr_geocollector.specimen_type_other',
            False,
        )
        if type_id:
            return type_id.id
        return False

    company_id = fields.Many2one(
        comodel_name='res.company',
        required=True,
        default=lambda self: self.env.company,
    )

    active = fields.Boolean(
        string='Active',
        default=True,
    )
    name = fields.Char(
        string='Code',
        required=True,
        copy=False,
        default=lambda self: _('New'),
    )
    type_id = fields.Many2one(
        string='Type',
        comodel_name='geo.specimen.type',
        required=True,
        default=_default_type_id,
    )

    note = fields.Html(
        string='Notes',
    )

    location_id = fields.Many2one(
        string='Discovery Location',
        comodel_name='geo.location',
    )
    partner_id = fields.Many2one(
        string='Found by',
        comodel_name='res.partner',
        default=lambda self: self.env.user.partner_id,
    )
    year_found = fields.Integer(
        string='Year Found',
    )
    month_found = fields.Selection(
        string='Month Found',
        selection=[
            ('01', _('January')),
            ('02', _('February')),
            ('03', _('March')),
            ('04', _('April')),
            ('05', _('May')),
            ('06', _('June')),
            ('07', _('July')),
            ('08', _('August')),
            ('09', _('September')),
            ('10', _('October')),
            ('11', _('November')),
            ('12', _('December')),
        ],
    )
    day_found = fields.Integer(
        string='Day found',
    )
    date_found = fields.Char(
        string='Date Found',
        compute='_compute_date_found',
    )

    date_added_to_collection = fields.Date(
        string='Date added to collection',
        default=lambda self: fields.Date.today(),
    )

    _sql_constraints = [
        (
            'name_unique',
            'unique (name)',
            'The code of the specimen must be unique!',
        ),
    ]

    @api.depends('year_found', 'month_found', 'day_found')
    def _compute_date_found(self):
        for record in self:
            year, month, day = record.year_found, record.month_found, record.day_found
            if not year:
                record.date_found = ''
                continue
            if not month:
                record.date_found = str(year)
                continue
            if not day:
                day = '__'

            record.date_found = f'{day}/{month}/{year}'

    def copy(self, default=None):
        default = default or {}
        if 'name' not in default:
            default['name'] = _('New')
        return super().copy(default)

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            if vals.get('name', '') != _('New'):
                continue
            company_id_int = vals.get('company_id')
            if company_id_int:
                company_id = self.env['res.company'].browse(company_id_int)
            else:
                company_id = self.env.company

            sequence_id = company_id.geo_specimen_sequence_id
            if sequence_id:
                new_name = sequence_id.next_by_id()
                vals['name'] = new_name

        return super().create(vals_list)
