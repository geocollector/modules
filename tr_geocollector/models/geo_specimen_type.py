from odoo import fields, models


class GeoSpecimenType(models.Model):
    _name = 'geo.specimen.type'
    _description = 'Specimen Type'

    name = fields.Char(
        string='Name',
        required=True,
        index=True,
        copy=False,
    )

    def copy(self, default=None):
        default = default or {}
        if 'name' not in default:
            default['name'] = f"{self.name} ({_('copy')})"
        return super().copy(default)

    _sql_constraints = [
        (
            "name_uniq", "unique(name)", (
                "A specimen status already exists with this name\n"
                "You cannot define another: please edit the existing one."
            ),
        ),
    ]
