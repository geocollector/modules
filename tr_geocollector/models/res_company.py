from odoo import fields, models


class ResCompany(models.Model):
    _inherit = 'res.company'

    geo_specimen_sequence_id = fields.Many2one(
        string='Specimen Sequence',
        comodel_name='ir.sequence',
    )
