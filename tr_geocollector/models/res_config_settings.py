from odoo import fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    geo_specimen_sequence_id = fields.Many2one(
        string='Specimen Sequence',
        comodel_name='ir.sequence',
        related='company_id.geo_specimen_sequence_id',
        readonly=False,
    )

    module_tr_geocollector_geology = fields.Boolean(
        string='Geocollector Geology',
    )
    module_tr_geocollector_trips = fields.Boolean(
        string='Trips',
    )
    module_tr_geocollector_collections = fields.Boolean(
        string='Collection',
    )
