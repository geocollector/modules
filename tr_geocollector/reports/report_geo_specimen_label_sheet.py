from odoo import api, models


class ReportGeoSpecimenLabelSheet(models.AbstractModel):
    _name = 'report.tr_geocollector.geo_specimen_label_sheet'
    _description = 'Print specimen label sheets'

    _LABELS_PER_PAGE = 2

    @api.model
    def _get_report_values(self, docids, data=None):
        docs = [
            {
                'specimen': specimen_id,
            }
            for specimen_id
            in self.env['geo.specimen'].browse(
                data.get('ids', []),
            )
        ]

        return {
            'doc_ids': self.ids,
            'doc_model': 'geo.specimen',
            'docs': {
                'sheets': tuple(
                    docs[i:i + self._LABELS_PER_PAGE]
                    for i
                    in range(0, len(docs), self._LABELS_PER_PAGE)
                ),
            },
        }
