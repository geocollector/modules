from odoo import fields, models


class PrintSpecimenLabels(models.TransientModel):
    _name = 'print.specimen.labels'
    _description = 'Print Specimen Labels'

    def default_geo_specimen_ids(self):
        active_ids = self.env.context.get('active_ids', [])
        return self.env['geo.specimen'].browse(active_ids)

    geo_specimen_ids = fields.Many2many(
        string='Specimens',
        comodel_name='geo.specimen',
        default=default_geo_specimen_ids,
    )

    def print_labels(self):
        return self.env.ref(
            'tr_geocollector.action_geo_specimen_label_sheet',
        ).report_action(
            self,
            data={
                'ids': self.geo_specimen_ids.ids,
            },
            config=False,
        )
