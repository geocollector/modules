# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 16.0.1.0.1 - 2023-09-01

### Changed

- Collection tab on specimen form view received a priority, to ensure order of tabs

## 16.0.1.0.0 - 2023-08-23

### Added

- Geo collections
- Method to automatically add new specimens to the collection of the current user
