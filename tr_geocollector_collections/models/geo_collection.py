from odoo import _, fields, models


class GeoCollection(models.Model):
    _name = 'geo.collection'
    _description = 'Collection'
    _check_company_auto = True

    company_id = fields.Many2one(
        comodel_name='res.company',
        required=True,
        default=lambda self: self.env.company,
    )

    partner_id = fields.Many2one(
        string='Owner',
        comodel_name='res.partner',
        default=lambda self: self.env.user.partner_id,
    )

    geo_specimen_ids = fields.One2many(
        string='Specimens',
        comodel_name='geo.specimen.collection',
        inverse_name='geo_collection_id',
    )

    def name_get(self):
        res = []
        for record in self:
            record_name = _('Collection %s', record.partner_id.display_name)
            res.append((record.id, record_name))

        return res
