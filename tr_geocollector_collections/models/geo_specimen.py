from datetime import datetime

from odoo import api, fields, models
from odoo.fields import Command


class GeoSpecimen(models.Model):
    _inherit = 'geo.specimen'

    geo_collection_ids = fields.One2many(
        string='Collections',
        comodel_name='geo.specimen.collection',
        inverse_name='geo_specimen_id',
    )

    @api.model_create_multi
    def create(self, vals_list):
        current_user_collection = self.env['geo.collection'].search(
            [
                ('partner_id', '=', self.env.user.partner_id.id),
            ],
            limit=1,
        )
        if current_user_collection:
            for vals in vals_list:
                if vals.get('geo_collection_ids'):
                    continue
                added_date = vals.get('date_added_to_collection')
                if not added_date:
                    continue
                added_date = datetime.strptime(added_date, '%Y-%m-%d')
                vals['geo_collection_ids'] = [
                    Command.create({
                        'geo_collection_id': current_user_collection.id,
                        'year_added': added_date.year,
                        'month_added': added_date.month,
                        'day_added': added_date.day,
                    }),
                ]

        return super().create(vals_list)
