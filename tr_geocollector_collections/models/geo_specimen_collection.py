from odoo import fields, models


class GeoSpecimenCollection(models.Model):
    _name = 'geo.specimen.collection'
    _description = 'Specimen Collection'
    _order = 'geo_specimen_id, geo_collection_id, sequence'
    _check_company_auto = True

    company_id = fields.Many2one(
        comodel_name='res.company',
        required=True,
        default=lambda self: self.env.company,
    )

    sequence = fields.Integer(default=1)

    geo_specimen_id = fields.Many2one(
        string='Specimen',
        comodel_name='geo.specimen',
        required=True,
    )
    geo_collection_id = fields.Many2one(
        string='Collection',
        comodel_name='geo.collection',
        required=True,
    )

    year_added = fields.Integer(
        string='Year Added',
    )
    month_added = fields.Integer(
        string='Month Added',
    )
    day_added = fields.Integer(
        string='Day Added',
    )
