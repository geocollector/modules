from odoo import fields, models


class ResCompany(models.Model):
    _inherit = 'res.company'

    geo_collection_id = fields.Many2one(
        string='Active Collection',
        comodel_name='geo.collection',
        domain="[('company_id', '=', id)]",
    )
