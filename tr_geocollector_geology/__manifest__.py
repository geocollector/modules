# GeoCollector: Manage your collection of rocks and fossils
# Copyright (C) 2023  Transistories

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


{
    'name': 'GeoCollector - Geology',
    'version': '16.0.1.1.0',
    'category': 'GeoCollector/GeoCollector',
    'summary': 'Manage your collection of rocks and fossils',
    'description': """""",
    'author': 'Transistories',
    'website': 'https://www.transistories.org',
    'license': 'GPL-3 or any later version',
    'depends': [
        'tr_geocollector',
        'tr_geology',
    ],
    'data': [
        'security/ir.model.access.csv',

        'views/geo_specimen.xml',
        'views/geo_specimen_mineral.xml',

        'views/menu_views.xml',

        'reports/report_geo_specimen_label_sheet.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': True,
}
