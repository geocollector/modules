from odoo import fields, models


class GeoSpecimen(models.Model):
    _inherit = 'geo.specimen'

    geo_mineral_ids = fields.One2many(
        string='Minerals',
        comodel_name='geo.specimen.mineral',
        inverse_name='geo_specimen_id',
    )

    def report_specimen_mineral_string(self):
        self.ensure_one()
        return ', '.join(
            self.geo_mineral_ids.mapped('geo_mineral_id.display_name')
        )
