from odoo import fields, models


class GeoSpecimenMineral(models.Model):
    _name = 'geo.specimen.mineral'
    _description = 'Specimen Mineral'

    geo_specimen_id = fields.Many2one(
        string='Specimen',
        comodel_name='geo.specimen',
        required=True,
    )

    geo_mineral_id = fields.Many2one(
        string='Mineral',
        comodel_name='geo.mineral.variety',
        required=True,
    )

    geo_crystal_system_id = fields.Many2one(
        string='Crystal System',
        comodel_name='geo.crystal.system',
    )
    geo_crystal_colour_id = fields.Many2one(
        string='Crystal Colour',
        comodel_name='geo.crystal.colour',
    )
    geo_crystal_translucency_id = fields.Many2one(
        string='Crystal Translucency',
        comodel_name='geo.crystal.translucency',
    )
