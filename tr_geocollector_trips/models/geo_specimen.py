from odoo import api, fields, models


class GeoSpecimen(models.Model):
    _inherit = 'geo.specimen'

    geo_trip_id = fields.Many2one(
        string='Trip',
        comodel_name='geo.trip',
    )

    @api.onchange('geo_trip_id')
    def _onchange_trip_set_info(self):
        for record in self:
            if record.geo_trip_id:
                record.location_id = record.geo_trip_id.destination_id
                record.year_found = record.geo_trip_id.trip_date.year
                record.month_found = str(
                    record.geo_trip_id.trip_date.month
                ).zfill(2)
                record.day_found = record.geo_trip_id.trip_date.day
            else:
                record.year_found = False
                record.month_found = False
                record.day_found = False

    @api.model_create_multi
    def create(self, vals_list):
        # TODO: Optimize
        records = super().create(vals_list)
        records._onchange_trip_set_info()
        return records

    def write(self, vals):
        res = super().write(vals)
        if not self.env.context.get('tr_geo_skip_specimen_set_trip'):
            self.with_context(
                tr_geo_skip_specimen_set_trip=True
            )._onchange_trip_set_info()
        return res
