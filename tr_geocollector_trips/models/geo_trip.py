from odoo import _, fields, models


class GeoTrip(models.Model):
    _name = 'geo.trip'
    _description = 'Geo Trip'
    _check_company_auto = True

    company_id = fields.Many2one(
        comodel_name='res.company',
        required=True,
        default=lambda self: self.env.company,
    )

    partner_id = fields.Many2one(
        string='Organizer',
        comodel_name='res.partner',
    )

    destination_id = fields.Many2one(
        string='Destination',
        comodel_name='geo.location',
        required=True,
    )
    trip_date = fields.Date(
        string='Date',
        default=lambda self: fields.Date.today(),
    )

    geo_specimen_ids = fields.One2many(
        string='Specimens',
        comodel_name='geo.specimen',
        inverse_name='geo_trip_id',
    )

    def name_get(self):
        res = [
            (
                record.id,
                _(
                    'Trip to %s on %s',
                    record.destination_id.name,
                    record.trip_date,
                ),
            )
            for record
            in self
        ]

        return res
